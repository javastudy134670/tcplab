package com.ouc.tcp.test;

import com.ouc.tcp.client.Client;
import com.ouc.tcp.message.TCP_PACKET;

import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * GBN内容
 * 窗口基类
 */
public abstract class WindowAbstract {
    protected Client client;
    /**
     * packet 队列
     * 线程安全
     */
    protected ConcurrentLinkedQueue<TCP_PACKET> packets = new ConcurrentLinkedQueue<>();
    /**
     * 原子操作整数
     */
    protected AtomicInteger size = new AtomicInteger(1);
    /**
     * TCP tahoe 添加
     * 慢开始阈值
     */
    protected AtomicInteger ssThresh;

    public WindowAbstract(Client client, int ssThresh) {
        this.client = client;
        this.ssThresh = new AtomicInteger(ssThresh);
    }

    public boolean isFull() {
        return packets.size() >= size.get();
    }

    /**
     * 超时重传
     */
    protected class ReSendTask extends TimerTask {
        @Override
        public void run() {
            System.err.println("@@@@@@@ Re transmit @@@@@@@");
            /*
            ssThresh >=2
             */
            System.out.printf("ssThresh %d->%d%n", ssThresh.get(), Math.max(size.get() / 2, 2));
            System.out.printf("size %d->%d%n", size.get(), 1);
            ssThresh.set(Math.max(size.get() / 2, 2));
            size.set(1);
            packets.forEach(client::send);
        }
    }

    @Override
    public String toString() {
        return "packets=" + packets;
    }
}
