package com.ouc.tcp.client;

import com.ouc.tcp.message.TCP_PACKET;
import com.ouc.tcp.test.TCP_Sender;
import com.ouc.tcp.tool.TCP_TOOL;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class ListenACK extends Thread {
    private final Client listenedClinet;
    private final TCP_Sender tcpSender;
    private long sysTimeMillis;
    private final SimpleDateFormat receiveTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS z");

    public ListenACK(Client client, TCP_Sender sender) {
        this.listenedClinet = client;
        this.tcpSender = sender;
    }

    public void run() {
        //noinspection InfiniteLoopStatement
        while(true) {
            try {
                this.listenedClinet.socket.receive(this.listenedClinet.receivePacket);
                this.sysTimeMillis = System.currentTimeMillis();
            } catch (IOException var4) {
                var4.fillInStackTrace();
            }

            TCP_PACKET recvPack = TCP_TOOL.getTCP_Packet(this.listenedClinet.receivePacket);
            int[] recvData = recvPack.getTcpS().getData();
            System.out.println("-> " + this.receiveTime.format(this.sysTimeMillis));
            System.out.println("** TCP_Sender");
            System.out.println("   Receive packet from: [" + recvPack.getSourceAddr().getHostAddress() + ":" + recvPack.getTcpH().getTh_sport() + "]");
            System.out.print("   Packet data:");

            for (int recvDatum : recvData) {
                System.out.print(" " + recvDatum);
            }

            System.out.println();
            if (TCP_TOOL.judgePacketType(recvPack) == 3) {
                System.out.println("   PACKET_TYPE: ACK_" + recvPack.getTcpH().getTh_ack());
                this.tcpSender.recv(recvPack);
            }
        }
    }
}
