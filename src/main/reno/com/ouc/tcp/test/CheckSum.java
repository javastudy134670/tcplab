package com.ouc.tcp.test;

import com.ouc.tcp.message.TCP_HEADER;
import com.ouc.tcp.message.TCP_PACKET;
import com.ouc.tcp.message.TCP_SEGMENT;

import java.util.zip.CRC32;

public class CheckSum {

    /*计算TCP报文段校验和：只需校验TCP首部中的seq、ack和sum，以及TCP数据字段*/
    public static short computeChkSum(TCP_PACKET tcpPack) {
        TCP_HEADER header = tcpPack.getTcpH();
        TCP_SEGMENT tcp_segment = tcpPack.getTcpS();
        CRC32 crc32 = new CRC32();
        crc32.update(header.getTh_seq());
        crc32.update(header.getTh_ack());
        for (int datum : tcp_segment.getData()) {
            crc32.update(datum);
        }
        return (short) crc32.getValue();
    }

}
