package com.ouc.tcp.test;

import com.ouc.tcp.client.TCP_Sender_ADT;
import com.ouc.tcp.message.TCP_PACKET;

import java.util.concurrent.CountDownLatch;

public class TCP_Sender extends TCP_Sender_ADT {

    /*
    GBN 删除曾经的Packet成员，替换成Window
    latch 当窗口满时阻塞的锁
     */
    private CountDownLatch latch = new CountDownLatch(1);
    /**
     * 为了方便调试，设置一个all ACK的标志
     */
    public volatile boolean finished = false;
    public final CountDownLatch finish = new CountDownLatch(1);
    private final SenderWindow senderWindow = new SenderWindow(client, 8);

    /*构造函数*/
    public TCP_Sender() {
        initTCP_Sender(this);
    }

    @Override
    //可靠发送（应用层调用）：封装应用层数据，产生TCP数据报；需要修改
    public void rdt_send(int dataIndex, int[] appData) {
        //生成TCP数据报（设置序号和数据字段/校验和),注意打包的顺序
        tcpH.setTh_seq(dataIndex * appData.length + 1);//包序号设置为字节流号：
        tcpS.setData(appData);
        TCP_PACKET tcpPack = new TCP_PACKET(tcpH, tcpS, destinAddr);
        //更新带有checksum的TCP 报文头
        tcpH.setTh_sum(CheckSum.computeChkSum(tcpPack));
        tcpPack.setTcpH(tcpH);
        /*
        GBN 内容
        如果window满了就先阻塞等等
         */
        if (senderWindow.isFull()) {
            System.err.println("Window is full, waiting...");
            latch = new CountDownLatch(1);
            try {
                latch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        try {
            senderWindow.putPacket(tcpPack.clone());
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        //发送TCP数据报
        udt_send(tcpPack);
    }

    @Override
    //不可靠发送：将打包好的TCP数据报通过不可靠传输信道发送；仅需修改错误标志
    public void udt_send(TCP_PACKET tcpPack) {
        //设置错误控制标志
        /*
        GBN 设定为7
        让发包有几率被延迟、出错、丢包
         */
        tcpH.setTh_eflag((byte) 7);
        client.send(tcpPack);
    }

    @Override
    //需要修改
    public void waitACK() {
    }

    /**
     * GBN 修改
     * 取消ackQueue和waitACK
     * 让window处理ack
     */
    @Override
    public void recv(TCP_PACKET recvPack) {
        if (CheckSum.computeChkSum(recvPack) == recvPack.getTcpH().getTh_sum()) {
            System.out.println("Receive ACK Number： " + recvPack.getTcpH().getTh_ack());
            System.out.println();
            /*
            GBN 取消了ackQueue， 将ack交给window，poll已经ack的包
             */
            senderWindow.receiveACK(recvPack.getTcpH().getTh_ack());
            if (finished && senderWindow.packets.isEmpty()) {
                /*
                用于检查结束
                 */
                finish.countDown();
            }
            if (!senderWindow.isFull()) {
                latch.countDown();
            }
        } else {
            /*
              GBN 取消了ackQueue，窗口会自动重发
             */
            System.out.println("Receive corrupt ACK" + recvPack.getTcpH().getTh_ack());
            System.out.println();
        }
    }

}
