package com.ouc.tcp.client;

import com.ouc.tcp.message.TCP_PACKET;
import java.util.TimerTask;

public class UDT_RetransTask extends TimerTask {
    private final Client senderClient;
    private final TCP_PACKET reTransPacket;

    public UDT_RetransTask(Client client, TCP_PACKET packet) {
        this.senderClient = client;
        this.reTransPacket = packet;
    }

    public void run() {
        this.senderClient.send(this.reTransPacket);
    }
}
