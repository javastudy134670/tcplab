package com.ouc.tcp.test;

import com.ouc.tcp.app.App_Sender;
import com.ouc.tcp.client.TCP_Receiver_ADT;
import com.ouc.tcp.message.TCP_PACKET;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class TCP_Receiver extends TCP_Receiver_ADT {

    int innerSequence = -1;//内部记录的包序列
    /*
    SR 内容
    模拟接收方窗口
    一次性可以接受窗口大小的多个data
     */
    int window_size = 25;
    HashMap<Integer, TCP_PACKET> pendingPackages = new HashMap<>();

    /*构造函数*/
    public TCP_Receiver() {
        initTCP_Receiver(this);    //初始化TCP接收端
    }

    @Override
    //接收到数据报：检查校验和，设置回复的ACK报文段
    public void rdt_recv(TCP_PACKET recvPack) {
        //回复的ACK报文段
        TCP_PACKET ackPack;
        //检查校验码，生成ACK
        if (CheckSum.computeChkSum(recvPack) == recvPack.getTcpH().getTh_sum()) {
            /*
            SR修改，加入接受者窗口机制，可以乱序收包
             */
            int nowSequence = (recvPack.getTcpH().getTh_seq() - 1) / App_Sender.dataGroupSize;//当前这个包的seq
            int offset = nowSequence - innerSequence;
            if (offset < window_size) {
                if (offset > 0 && !pendingPackages.containsKey(nowSequence)) {
                    /*
                    已经收到过的包不必重复交付，未收到过的包挂起并尝试交付
                     */
                    System.out.println("Receiver: pending package; " + recvPack.getTcpH().getTh_seq());
                    pendingPackages.put(nowSequence, recvPack);
                    /*
                    尝试将个包移出挂起列表，交付
                     */
                    int index = 1;
                    while (pendingPackages.get(innerSequence + index) != null) {
                        dataQueue.add(pendingPackages.get(innerSequence + index).getTcpS().getData());
                        pendingPackages.put(innerSequence + index, null);
                        index += 1;
                    }
                    /*
                    innerSequence更新为最新交付的包序列
                     */
                    innerSequence += index - 1;
                }
                /*
                窗口上限以内的都可以ACK
                 */
                tcpH.setTh_ack(recvPack.getTcpH().getTh_seq());
            } else {
                /*
                超过窗口范围的包拒收
                 */
                System.err.println("Receiver: deny to receive this package which exceeded the window");
                System.err.println("seq=" + nowSequence);
                tcpH.setTh_ack(innerSequence * App_Sender.dataGroupSize + 1);
            }
        } else {
            System.out.println("Receive Computed: " + CheckSum.computeChkSum(recvPack));
            System.out.println("Received Packet" + recvPack.getTcpH().getTh_sum());
            System.out.println("Problem: Packet Number: " + recvPack.getTcpH().getTh_seq() + " + InnerSeq:  " + innerSequence);
            /*
            RDT2.2修改内容
            错误包返回ACK的seq值不再是-1，而是上次正确的ACK的值
             */
            tcpH.setTh_ack(innerSequence * App_Sender.dataGroupSize + 1);
        }
        System.out.println();
        ackPack = new TCP_PACKET(tcpH, tcpS, recvPack.getSourceAddr());
        tcpH.setTh_sum(CheckSum.computeChkSum(ackPack));
        //回复ACK报文段
        reply(ackPack);

        //交付数据（每20组数据交付一次）
        if (dataQueue.size() == 20)
            deliver_data();
    }

    @Override
    //交付数据（将数据写入文件）；不需要修改
    public void deliver_data() {
        //检查dataQueue，将数据写入文件
        File fw = new File("recvData.txt");
        BufferedWriter writer;

        try {
            writer = new BufferedWriter(new FileWriter(fw, true));

            //循环检查data队列中是否有新交付数据
            while (!dataQueue.isEmpty()) {
                int[] data = dataQueue.poll();

                //将数据写入文件
                for (int datum : data) {
                    writer.write(datum + "\n");
                }

                writer.flush();        //清空输出缓存
            }
            writer.close();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
    }

    @Override
    //回复ACK报文段
    public void reply(TCP_PACKET replyPack) {
        /*
        GBN 设置为7
        可能错误、延迟、丢包
         */
        tcpH.setTh_eflag((byte) 7);
        //发送数据报
        client.send(replyPack);
    }

}
