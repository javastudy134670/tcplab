package com.ouc.tcp.server;

import com.ouc.tcp.message.TCP_PACKET;
import java.util.Random;

public class ForwardDelay extends Thread {
    private final TCP_PACKET delayPack;

    public ForwardDelay(TCP_PACKET delayPack) {
        this.delayPack = delayPack;
    }

    public void run() {
        Random timeRand = new Random();
        long delay = timeRand.nextInt(20001) + 20000;

        try {
            Thread.sleep(delay);
            Server.forwardMessage(this.delayPack);
        } catch (InterruptedException var5) {
            var5.fillInStackTrace();
        }

    }
}
