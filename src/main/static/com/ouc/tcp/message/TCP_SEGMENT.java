package com.ouc.tcp.message;

import java.io.Serializable;

public class TCP_SEGMENT implements Serializable, Cloneable {
    private int[] tcpData;

    public TCP_SEGMENT() {
        this.tcpData = new int[0];
    }

    public TCP_SEGMENT(int l, int r) {
        if (l == r) {
            this.tcpData = new int[0];
        } else {
            this.tcpData = new int[Math.abs(r - l + 1)];
            if (l > r) {
                l = r;
            }

            for(int i = 0; i < this.tcpData.length; ++i) {
                this.tcpData[i] = l + i;
            }
        }

    }

    public TCP_SEGMENT(int[] data) {
        this.tcpData = data.clone();
    }

    public TCP_SEGMENT clone() throws CloneNotSupportedException {
        super.clone();
        return new TCP_SEGMENT(this.tcpData.clone());
    }

    public int[] getData() {
        return this.tcpData;
    }

    public void setData(int l, int r) {
        if (l == r) {
            this.tcpData = new int[0];
        } else {
            this.tcpData = new int[Math.abs(r - l + 1)];
            if (l > r) {
                l = r;
            }

            for(int i = 0; i < this.tcpData.length; ++i) {
                this.tcpData[i] = l + i;
            }
        }

    }

    public void setData(int[] data) {
        this.tcpData = data.clone();
    }

    public void setDataByIndex(int index, int data) {
        this.tcpData[index] = data;
    }

    public int getDataLengthInByte() {
        return this.tcpData.length * 4;
    }
}
