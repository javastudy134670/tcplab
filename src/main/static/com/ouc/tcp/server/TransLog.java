package com.ouc.tcp.server;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class TransLog {
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS z");
    private final ArrayList<String> transitTime = new ArrayList<>();
    private final ArrayList<Integer> packType = new ArrayList<>();
    private final ArrayList<Integer> transStatus = new ArrayList<>();
    private final ArrayList<Integer> seq = new ArrayList<>();
    private final ArrayList<Integer> ack = new ArrayList<>();
    private final ArrayList<Boolean> checkResponse = new ArrayList<>();
    private final ArrayList<Boolean> retransFlag = new ArrayList<>();

    public TransLog() {
    }

    public void addLog(int packType, int transStatus, int seq, int ack) {
        long sysTimeMillis = System.currentTimeMillis();
        this.transitTime.add(this.timeFormat.format(sysTimeMillis));
        int logIndex;
        if (packType == 0) {
            logIndex = this.seq.lastIndexOf(seq);
        } else {
            logIndex = this.ack.lastIndexOf(ack);
        }

        if (logIndex != -1) {
            if (this.packType.get(logIndex) == packType && !(Boolean)this.checkResponse.get(logIndex)) {
                this.retransFlag.add(true);
            } else {
                this.retransFlag.add(false);
            }
        } else {
            this.retransFlag.add(false);
        }

        this.packType.add(packType);
        this.transStatus.add(transStatus);
        this.seq.add(seq);
        this.ack.add(ack);
        if (packType == 1 && transStatus == 0) {
            this.checkResponse.add(true);
        } else {
            this.checkResponse.add(false);
        }

    }

    public void checkDataACK(int responseAck) {
        for(int i = this.seq.size() - 1; i >= 0; --i) {
            if (this.packType.get(i) == 0 && this.seq.get(i) == responseAck && !(Boolean)this.checkResponse.get(i) && this.transStatus.get(i) == 0) {
                this.checkResponse.set(i, true);
                break;
            }
        }

    }

    public int getTotalPackNum() {
        return this.seq.size();
    }

    public int[] countStatus() {
        int[] status = new int[4];
        Arrays.fill(status, 0);

        for (Integer integer : this.transStatus) {
            ++status[integer];
        }

        return status;
    }

    public double getTransSucRatio() {
        double sucN = 0.0;

        for(int i = 0; i < this.seq.size(); ++i) {
            if (this.transStatus.get(i) == 0 && this.checkResponse.get(i)) {
                ++sucN;
            }
        }

        return sucN / (double) this.seq.size();
    }

    public String toString() {
        StringBuilder logList = new StringBuilder();
        String statusStr = "";
        String responseStr = "";

        for(int i = 0; i < this.seq.size(); ++i) {
            logList.append("\t");
            logList.append(this.transitTime.get(i));
            logList.append("\t");
            if (this.retransFlag.get(i)) {
                logList.append("*Re: ");
            }

            if (this.packType.get(i) == 0) {
                logList.append("DATA_seq: ");
                logList.append(this.seq.get(i));
                if (this.checkResponse.get(i)) {
                    responseStr = "ACKed";
                } else {
                    responseStr = "NO_ACK";
                }
            } else {
                logList.append("ACK_ack: ");
                logList.append(this.ack.get(i));
            }

            switch (this.transStatus.get(i)) {
                case 0:
                    statusStr = "";
                    break;
                case 1:
                    statusStr = "WRONG";
                    break;
                case 2:
                    statusStr = "LOSS";
                    break;
                case 3:
                    statusStr = "DELAY";
            }

            logList.append("\t");
            logList.append(statusStr);
            if (this.packType.get(i) == 0) {
                logList.append("\t");
                logList.append(responseStr);
            }

            logList.append("\n");
        }

        return logList.toString();
    }
}
