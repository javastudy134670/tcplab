package com.ouc.tcp.config;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class SYS_INI {
    private static Properties ini = null;
    private static final File file = new File("Config.ini");

    static {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            ini = new Properties();
            ini.load(inputStream);
        } catch (Exception var1) {
            var1.fillInStackTrace();
        }

    }

    public SYS_INI() {
    }

    public static String getIniKey(String key) {
        return !ini.containsKey(key) ? "" : ini.get(key).toString();
    }

    public static void setIniKey(String key, String value) {
        if (ini.containsKey(key)) {
            ini.put(key, value);
        }
    }
}
