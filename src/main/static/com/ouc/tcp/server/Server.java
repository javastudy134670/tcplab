package com.ouc.tcp.server;

import com.ouc.tcp.config.Constant;
import com.ouc.tcp.message.MSG_STREAM;
import com.ouc.tcp.message.TCP_PACKET;
import com.ouc.tcp.tool.TCP_TOOL;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Server {
    private static DatagramSocket socket;
    private static DatagramPacket newPacket;
    private final int ECHOMAX = 65600;
    private static ArrayList<String> clientHost;
    private static ArrayList<TransLog> transLog;

    public Server() {
        InetAddress ipAddr = Constant.LocalAddr;
        int servPort = Constant.ServerPort;

        try {
            socket = new DatagramSocket(servPort, ipAddr);
            newPacket = new DatagramPacket(new byte[65600], 65600);
        } catch (SocketException var2) {
            var2.fillInStackTrace();
        }

        clientHost = new ArrayList<>();
        transLog = new ArrayList<>();
    }

    public static void forwardMessage(TCP_PACKET tcpPack) {
        MSG_STREAM msgStr = new MSG_STREAM(tcpPack);
        byte[] packStr = msgStr.getPacket_byteStream();
        InetAddress destinAddr = tcpPack.getDestinAddr();
        int destinPort = tcpPack.getTcpH().getTh_dport();
        DatagramPacket forwardPacket = new DatagramPacket(packStr, packStr.length, destinAddr, destinPort);

        try {
            socket.send(forwardPacket);
        } catch (IOException var6) {
            var6.fillInStackTrace();
        }

    }

    public void printServerSocketAddress() {
        String socketAddr = socket.getLocalSocketAddress().toString();
        System.out.println(socketAddr);
    }

    private static void listenNewPacket() {
        Random errorRand = new Random();
        Timer timer = new Timer();
        TimerTask writeLog = new TimerTask() {
            public void run() {
                (new WriteLogFile(Server.clientHost, Server.transLog)).start();
            }
        };
        timer.schedule(writeLog, 5000L, 5000L);

        //noinspection InfiniteLoopStatement
        while (true) {
            try {
                socket.receive(newPacket);
                TCP_PACKET tcpPack = TCP_TOOL.getTCP_Packet(newPacket);
                if (TCP_TOOL.judgePacketType(tcpPack) < 2) {
                    forwardMessage(tcpPack);
                } else {
                    int eFlag = errorRand.nextInt(100);
                    if (eFlag == 1) {
                        int eType;
                        switch (tcpPack.getTcpH().getTh_eflag()) {
                            case 0:
                                eType = 0;
                                break;
                            case 1:
                                eType = 1;
                                break;
                            case 2:
                                eType = 2;
                                break;
                            case 3:
                                eType = 3;
                                break;
                            case 4:
                                eType = errorRand.nextInt(2) + 1;
                                break;
                            case 5:
                                eType = errorRand.nextInt(2) + 1;
                                if (eType == 2) {
                                    ++eType;
                                }
                                break;
                            case 6:
                                eType = errorRand.nextInt(2) + 2;
                                break;
                            default:
                                eType = errorRand.nextInt(3) + 1;
                        }

                        if (eType == 0) {
                            forwardMessage(tcpPack);
                        } else {
                            processTransErr(tcpPack, eType);
                        }

                        makeLog(tcpPack, eType);
                    } else {
                        forwardMessage(tcpPack);
                        makeLog(tcpPack, 0);
                    }
                }
            } catch (IOException var7) {
                var7.fillInStackTrace();
            }
        }
    }

    private static String getClientHost(TCP_PACKET tcpPack, int clientFlag) {
        return clientFlag == 0 ? tcpPack.getSourceAddr().getHostAddress() + ":" + tcpPack.getTcpH().getTh_sport() : tcpPack.getDestinAddr().getHostAddress() + ":" + tcpPack.getTcpH().getTh_dport();
    }

    private static void processTransErr(TCP_PACKET tcpPack, int eType) {
        Random errorSet = new Random();
        switch (eType) {
            case 1:
                if (tcpPack.getTcpS().getData().length > 0) {
                    int dataLen = tcpPack.getTcpS().getData().length;
                    int eNum = dataLen / 100;
                    if (eNum < 1) {
                        eNum = 1;
                    }

                    for (int i = 0; i < eNum; ++i) {
                        int eOffset = errorSet.nextInt(dataLen);
                        tcpPack.getTcpS().setDataByIndex(eOffset, -(errorSet.nextInt(Integer.MAX_VALUE) + 1));
                    }
                } else {
                    tcpPack.getTcpH().setTh_ack(-(errorSet.nextInt(Integer.MAX_VALUE) + 1));
                }

                forwardMessage(tcpPack);
            case 2:
            default:
                break;
            case 3:
                (new ForwardDelay(tcpPack)).start();
        }

    }

    private static void makeLog(TCP_PACKET tcpPack, int transFlag) {
        String sHost = getClientHost(tcpPack, 0);
        String dHost = getClientHost(tcpPack, 1);
        if (!clientHost.contains(sHost)) {
            clientHost.add(sHost);
            transLog.add(new TransLog());
        }

        byte packType;
        if (TCP_TOOL.judgePacketType(tcpPack) == 2) {
            packType = 0;
        } else {
            packType = 1;
        }

        int sHostIndex = clientHost.indexOf(sHost);
        transLog.get(sHostIndex).addLog(packType, transFlag, tcpPack.getTcpH().getTh_seq(), tcpPack.getTcpH().getTh_ack());
        if (packType == 1 && transFlag == 0 && clientHost.contains(dHost)) {
            int dHostIndex = clientHost.indexOf(dHost);
            transLog.get(dHostIndex).checkDataACK(tcpPack.getTcpH().getTh_ack());
        }

    }

    public static void main(String[] args) {
        Server server = new Server();
        System.out.print("Server socket address: ");
        server.printServerSocketAddress();
        System.out.println("** Server: Listening channel...\n");
        listenNewPacket();
    }
}
