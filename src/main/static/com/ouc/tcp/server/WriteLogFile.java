package com.ouc.tcp.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;

public class WriteLogFile extends Thread {
    private final ArrayList<String> clientHost;
    private final ArrayList<TransLog> transLog;

    public WriteLogFile(ArrayList<String> host, ArrayList<TransLog> log) {
        this.clientHost = host;
        this.transLog = log;
    }

    public void run() {
        File fw = new File("Log.txt");

        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(fw, false));
            output.write("CLIENT HOST\tTOTAL\tSUC_RATIO\tNORMAL\tWRONG\tLOSS\tDELAY\n");

            for(int i = 0; i < this.clientHost.size(); ++i) {
                output.write(this.clientHost.get(i));
                output.write("\t" + this.transLog.get(i).getTotalPackNum());
                output.write("\t" + formatPercent(this.transLog.get(i).getTransSucRatio()));
                int[] transStatus = this.transLog.get(i).countStatus();

                for (int status : transStatus) {
                    output.write("\t" + status);
                }

                output.write("\n" + this.transLog.get(i).toString() + "\n");
            }

            output.close();
        } catch (IOException var6) {
            var6.fillInStackTrace();
        }

    }

    private static String formatPercent(double k) {
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMinimumFractionDigits(2);
        return nf.format(k);
    }
}
