package com.ouc.tcp.app;

import com.ouc.tcp.test.TCP_Sender;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import sun.misc.BASE64Decoder;

public class App_Sender {
    public static final int dataGroupSize = 100;
    private static Key key;
    private static final int[] appData = new int[dataGroupSize];
    private static final TCP_Sender tcpSender = new TCP_Sender();

    public App_Sender() {
    }

    public static void main(String[] args) {
        System.out.println("** TCP_Sender: Press enter key to start data transmission...");

        try {
            int result = System.in.read();
            if (result == -1) return;
        } catch (IOException var7) {
            var7.fillInStackTrace();
        }

        getKey();
        File fr = new File("ENCDA.tcp");
        String encStr;
        String dataStr;
        int dataNum = 0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fr));

            while ((encStr = reader.readLine()) != null) {
                dataStr = getDesString(encStr);
                appData[dataNum++ % dataGroupSize] = Integer.parseInt(dataStr);
                if (dataNum % dataGroupSize == 0) {
                    tcpSender.rdt_send((dataNum - 1) / dataGroupSize, appData);
                    Thread.sleep(10L);
                }
            }

            reader.close();
            System.err.println("\n**************** TCP_Sender: Data sending ends. ****************\n");
            tcpSender.finished = true;
            tcpSender.finish.await();
            System.err.println("****************** ALL ACK!! ************************");
        } catch (InterruptedException | IOException var8) {
            var8.fillInStackTrace();
        }

    }

    private static void getKey() {
        try {
            KeyGenerator _generator = KeyGenerator.getInstance("DES");
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed("OUCnet2012$#@!".getBytes());
            _generator.init(secureRandom);
            key = _generator.generateKey();
        } catch (Exception var3) {
            var3.fillInStackTrace();
        }

    }

    private static String getDesString(String strMi) {
        BASE64Decoder base64De = new BASE64Decoder();
        byte[] byteMing;
        byte[] byteMi;
        String strMing = "";

        try {
            byteMi = base64De.decodeBuffer(strMi);
            byteMing = getDesCode(byteMi);
            strMing = new String(byteMing, StandardCharsets.UTF_8);
        } catch (Exception var9) {
            var9.fillInStackTrace();
        }

        return strMing;
    }

    private static byte[] getDesCode(byte[] byteD) {
        byte[] byteFina = null;

        Cipher cipher;
        try {
            cipher = Cipher.getInstance("DES");
            cipher.init(2, key);
            byteFina = cipher.doFinal(byteD);
        } catch (Exception var7) {
            var7.fillInStackTrace();
        }

        return byteFina;
    }
}
