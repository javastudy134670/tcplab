package com.ouc.tcp.tool;

import com.ouc.tcp.message.TCP_PACKET;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class TCP_TOOL {
    public static final int MAXSEQ = Integer.MAX_VALUE;

    public TCP_TOOL() {
    }

    public static InetAddress getLocalIpAddr() {
        try {
            Enumeration<NetworkInterface> interfaceList = NetworkInterface.getNetworkInterfaces();
            if (interfaceList == null) {
                return null;
            }

            while (interfaceList.hasMoreElements()) {
                NetworkInterface iface = interfaceList.nextElement();
                Enumeration<InetAddress> addrList = iface.getInetAddresses();

                while (addrList.hasMoreElements()) {
                    InetAddress address = addrList.nextElement();
                    if (address instanceof Inet4Address && !address.isLoopbackAddress()) {
                        return address;
                    }
                }
            }
        } catch (SocketException var4) {
            System.out.println("Error to get network interface: " + var4.getMessage());
        }

        return null;
    }

    public static TCP_PACKET getTCP_Packet(DatagramPacket packet) {
        TCP_PACKET tcpPack = null;
        byte[] packetByte = packet.getData();
        ByteArrayInputStream bAIStream = new ByteArrayInputStream(packetByte);

        try {
            ObjectInputStream oIStream = new ObjectInputStream(bAIStream);
            tcpPack = (TCP_PACKET) oIStream.readObject();
            oIStream.close();
        } catch (IOException | ClassNotFoundException var6) {
            var6.fillInStackTrace();
        }

        return tcpPack;
    }

    public static int judgePacketType(TCP_PACKET tcpPack) {
        if (tcpPack.getTcpH().getTh_flags_SYN() && !tcpPack.getTcpH().getTh_flags_ACK()) {
            return 0;
        } else if (tcpPack.getTcpH().getTh_flags_SYN() && tcpPack.getTcpH().getTh_flags_ACK()) {
            return 1;
        } else if (tcpPack.getTcpH().getTh_flags_ACK() && !tcpPack.getTcpH().getTh_flags_SYN()) {
            return tcpPack.getTcpS().getData().length > 0 ? 2 : 3;
        } else {
            return -1;
        }
    }
}
