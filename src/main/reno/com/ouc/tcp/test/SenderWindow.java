package com.ouc.tcp.test;

import com.ouc.tcp.client.Client;
import com.ouc.tcp.client.UDT_Timer;
import com.ouc.tcp.message.TCP_PACKET;

/**
 * GBN内容
 * 发送窗口
 */
public class SenderWindow extends WindowAbstract {
    private UDT_Timer timer;
    /**
     * tahoe
     * 为了实现快重传，记录上一次的序列并记录重复次数
     */
    private int last_seq = 0;
    private int repeat = 1;

    public SenderWindow(Client client, int ssThresh) {
        super(client, ssThresh);
    }

    public void putPacket(TCP_PACKET packet) {
        boolean empty = packets.isEmpty();
        packets.add(packet);
        if (empty) {
            timer = new UDT_Timer();
            timer.schedule(new ReSendTask(), 3000, 3000);
        }
    }

    public void receiveACK(int sequence) {
        /*
        tahoe
        快重传逻辑
         */
        if (sequence == last_seq) {
            repeat += 1;
            if (repeat >= 3) {
                System.err.println("same ACK happened for " + repeat + " times");
                System.err.println("@@@@@@@ fast recovery @@@@@@@");
                /*
                reno 升级成快恢复
                 */
                System.out.printf("ssThresh %d->%d%n", ssThresh.get(), Math.max(size.get() / 2, 2));
                ssThresh.set(Math.max(size.get() / 2, 2));
                System.out.printf("size %d->%d%n", size.get(), ssThresh.get() + 3);
                size.set(ssThresh.get() + 3);
                timer.cancel();
                new Thread(() -> packets.forEach(client::send)).start();
                timer = new UDT_Timer();
                timer.schedule(new ReSendTask(), 3000, 3000);
            }
        } else {
            last_seq = sequence;
            repeat = 1;
        }
        /*
        GBN
       ack可能会延迟丢包，所以只能一个一个按顺序的ack
         */
        if (!packets.isEmpty() && packets.peek().getTcpH().getTh_seq() == sequence) {
            packets.poll();
            timer.cancel();
            if (!packets.isEmpty()) {
                timer = new UDT_Timer();
                timer.schedule(new ReSendTask(), 3000, 3000);
            }
            if (size.get() < ssThresh.get()) {
                System.out.println("@@@@@slow start size=" + size.get() + " ssThresh=" + ssThresh);
                size.set(2 * size.get());
            } else {
                System.out.println("@@@@@congestion avoid size=" + size.get() + " ssThresh=" + ssThresh);
                size.incrementAndGet();
            }
        }
    }

}
